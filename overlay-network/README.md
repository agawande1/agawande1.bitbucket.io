Running the program
===================

There are three options to run:

0) User my go directly to https://agawande1.bitbucket.io/overlay-network/.

1) Or simply open the index.html file in a browser.

2) Use this option if there is any trouble with loading the javscript in option 1.

To run the project, issue the following command in the project's directory:

```python -m SimpleHTTPServer```

and then point your browser to:

```localhost:8000```

Python server simply lets one serve javascript without using some webserver such as Apache.


Dependencies
============

The program uses sigmajs library which is placed in the js/ folder.
Sigmajs is used to display the real network. Real network nodes have randomly chosen coordinates.

The program uses visjs library which is placed in vij/ folder. This was not used for real network
as has its own algorithm to place nodes which takes a long time to display everything for very large networks.
However since the overlay contains far fewer links than the real network it can display the overlay
network beautifully figuring out the layout on its own.For bigger topologies it will take time some
to draw the graph as it figures out the layout. For example it may take 15-30 seconds to draw a 200 node simulation.
Visjs also supports zooming much better (less sensitive) than sigmajs. So the overlay networks can be zoomed into
to see the nodes of the topology.

The program uses graphlib for Dijkstra's algorithm.

Walkthrough
===========

index.html : Contains all the css, javascript and html for the simulation.
             (Need to refactor)

Custom clustering algorithm based on the position of the nodes is used.
The position of the nodes are generated randomly in the real network.

Dijkstra's algorithm is used to figure out the route.

Problems
========

For very large topologies - simulation will be CPU intensive.
