/*
	This class is based on the paper's pseudo code (Stoica et. al. 2003).
	I used the same function names as given there and supplied the additional
	functionality needed to make it work. 
	I was not able to get the bugs of Finger table resolved so I implemented
	a simple key lookup - O(N). So node state is O(1).
*/

class Node {
	constructor(m, id) {
		this.m = m;
		this.id = id;
		this.succ = null;
		this.pred = null;
		this.createList = true;
	}

	create() {
		this.succ = this;
	}

	join(node) {
		this.pred = null;
		this.createList = false;
		this.succ = node.find_successor(this.id);
		this.createList = true;
	}

	find_successor(ident, i) {
		/*
		 * self is the node who is joining the network
		 * self is the node where the key (identity) is being searched from
		 * This function finds the successor node of this identity
		*/
		if (this.createList) {
		  traverseList.push(this.id); // To store the patch - so that it can be later used for animation
	    }

        // This is how you check whether s is a default argument in Java script
        i = (typeof i !== 'undefined') ?  i : 0;

		i += 1;

		// Other wise maximum recursion depth will be exceeded
		if (i > 2*this.m) {
			return this.succ;
		}

		if (this == this.succ ||
			this.belongsToIncluded(this.id, this.succ.id, ident)) {
			return this.succ;
	    }
		else {
			return this.succ.find_successor(ident, i);
		}
	}

	belongsToIncluded(a, bI, ident) {
		if (a > bI) {
			if (ident <= bI || ident > a) {
				return true;
			}
		}
		else {
			if (ident > a && ident <= bI) {
				return true;
			}
		}
		return false;
	}

	belongsTo(a, b, ident) {
		if (a > b) {
			if (ident < b || ident > a) {
				return true;
			}
		}
		else {
			if (ident > a && ident < b) {
				return true;
			}
		}
		return false;
	}

	stabilize() {
		let x = this.succ.pred
		if (x != null && (this == this.succ || this.belongsTo(this.id, this.succ.id, x.id))) {
			this.succ = x;
		}
		this.succ.notify(this);
	}

	notify(node) {
		if (this.pred == null || this.belongsTo(this.pred.id, this.id, node.id)) {
			this.pred = node;
		}
	}
}